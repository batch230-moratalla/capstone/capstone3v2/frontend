import { Link, useLocation, useNavigate } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { useContext, useState, useEffect } from 'react';
import Axios from 'axios';
import ShopContext from '../ShopContext';
import Swal from 'sweetalert2';

const LoginPage = () => {
  const navigate = useNavigate();
  const { search } = useLocation();
  const redirectInUrl = new URLSearchParams(search).get('redirect');
  const redirect = redirectInUrl ? redirectInUrl : '/';

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const { state, dispatch: contextDispatch } = useContext(ShopContext);
  const { userInfo } = state;
  const submit = async (event) => {
    event.preventDefault();

    try {
      const { data } = await Axios.post(
        `${process.env.REACT_APP_API_URL}/api/users/login`,
        {
          email,
          password,
        }
      );

      // const { data } = await Axios.post('/api/users/login', {
      //   email,
      //   password,
      // });

      // const { data } = fetch(
      //   `${process.env.REACT_APP_API_URL}/api/users/login`,
      //   {
      //     method: 'POST',
      //     headers: { 'Content-Type': 'application/json' },
      //     body: JSON.stringify({
      //       email: email,
      //       password: password,
      //     }),
      //   }
      // );
      contextDispatch({ type: 'USER_LOGIN', payload: data });
      localStorage.setItem('userInfo', JSON.stringify(data));
      Swal.fire({
        title: 'Login Successful',
        icon: 'success',
        text: `Welcome ${data.name}`,
      });
      navigate(redirect || '/');
    } catch (error) {
      Swal.fire({
        title: 'Unable to Login',
        icon: 'error',
        text: 'Please check your email and password.',
      });
    }
  };

  useEffect(() => {
    if (userInfo) {
      navigate(redirect);
    }
  }, [navigate, redirect, userInfo]);
  return (
    <Container className="small-container">
      <h1 className="my-3">Login</h1>
      <Form onSubmit={submit}>
        <Form.Group className="mb-3" controlId="email">
          <Form.Label>Email</Form.Label>
          <Form.Control
            type="email"
            required
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            required
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>
        <div className="mb-3">
          <Button type="submit">Login</Button>
        </div>
        <div className="mb-3">
          No Account yet?{' '}
          <Link to={`/register?redirect=${redirect}`}>Create your account</Link>
        </div>
      </Form>
    </Container>
  );
};

export default LoginPage;
