import React from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';
import Rating from './Rating';
// import '../index.css';
import axios from 'axios';
import { useContext } from 'react';
import ShopContext from '../ShopContext';
// import Alert from '@mui/material/Alert';
import Swal from 'sweetalert2';

const Product = (props) => {
  const { product } = props;
  const { state, dispatch: contextDispatch } = useContext(ShopContext);

  const addToCart = async () => {
    const { cart } = state;
    const itemExist = cart.cartItems.find((item) => item._id === product._id);
    const quantity = itemExist ? itemExist.quantity + 1 : 1;
    const { data } = await axios.get(`/api/products/${product._id}`);
    if (data.stock < quantity) {
      // window.alert('No more Available Stock');
      // Swal.fire({
      //   title: 'Out of Stock',
      //   icon: 'error',
      //   text: 'We apologize for the inconvinience.',
      // });
      Swal.fire({
        title: 'Out of Stock',
        imageUrl: '/images/nomore.png',
        imageWidth: 600,
        imageHeight: 200,
        imageAlt: 'Custom image',
        icon: 'error',
      });
      return;
    }
    contextDispatch({
      type: 'CART_ADD_ITEM',
      payload: { ...product, quantity },
    });
  };
  return (
    <Card className="product">
      <Link to={`/product/${product.slug}`}>
        <img src={product.image} className="card-img-top" alt={product.name} />
      </Link>
      <Card.Body className="bg-card">
        <Link to={`/product/${product.slug}`}>
          <Card.Title>{product.name}</Card.Title>
        </Link>
        {/* <Rating rating={product.rating} reviews={product.reviews} /> */}
        <Card.Text>₱{product.price}</Card.Text>
        {product.stock === 0 ? (
          <Button variant="light" disabled>
            Out of stock
          </Button>
        ) : (
          <Button onClick={() => addToCart(product)}>Add to cart</Button>
        )}
      </Card.Body>
    </Card>
  );
};

export default Product;
